const express = require("express");
const app = express();
const morgan = require("morgan");
var movimientosJSONv2 = require("./movimientos.json");
var usuarios = require("./usuarios.json");

//Setings
app.set('port',process.env.PORT || 3000);

//midlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false }));
app.use(express.json());

app.use(function(req, res, next) {
    // Instead of "*" you should enable only specific origins
    res.header('Access-Control-Allow-Origin', '*');
    // Supported HTTP verbs
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    // Other custom headers
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

//rutas
//app.get('/',(req,res)=>{
//  res.json({"Title": "hola mundo"});
//});

//app.get('/usuarios/:id',(req,res)=>{
  //  res.send(usuarios[req.params.id]);
//});

//app.get('/usuarios/:correo',(req,res)=>{
  //  var usuario = usuarios.filter(function(m){
    //    return m.correo === req.params.correo;
    //})[0];
    //res.send(usuario);
//});

app.get('/usuarios/:correo/:password',function (req, res) {
    var respuesta = {nombre: 'Contraseña o usuario incorrectos'}
    var usuario = usuarios.filter(function(m){return m.correo === req.params.correo;})[0];
        if (usuario.password === req.params.password){
            res.send(usuario);
        }else{
            res.send(respuesta);
        }
})

app.get('/usuarios',(req,res)=>{
    res.json(usuarios);
});

app.get('/movimientos',(req,res)=>{
    res.json(movimientosJSONv2);
});

app.get('/movimientos/:id',(req,res)=>{
    res.send(movimientosJSONv2[req.params.id - 1]);
});


app.post('/movimientos',(req,res)=>{
    const id = movimientosJSONv2.length +1;
    const newmov = {...req.body,id};
    movimientosJSONv2.push(newmov);
    res.send('Resivido');
});

app.post('/usuarios1',(req,res)=>{
    const id = movimientosJSONv2.length +1;
    const newmov = {...req.body,id};
    var respuesta = {nombre: 'Registrado correctamente'}
    usuarios.push(newmov);
    res.send(respuesta);
});


//iniciando el servidor 
app.listen(3000, () => {
    console.log('Server on port ${3000}');
});